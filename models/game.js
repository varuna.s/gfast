const mongoose = require('mongoose');
const dayjs = require('dayjs').extend(require('dayjs/plugin/isSameOrBefore'));


//mongoose schema for game
const gameSchema = new mongoose.Schema({
    name: {
        type: String,
        required: [true, 'name is required'],
        minlength: 1,
        maxlength: 150
    },
    genre: {
        type: String,
        required: [true, 'genre is required'],
        minlength: 1,
        maxlength: 150
    },
    developerName: {
        type: String,
        required: [true, 'developer name is required'],
        minlength: 1,
        maxlength: 150
    },
    publisherName: {
        type: String,
        required: [true, 'publisher name is required'],
        minlength: 1,
        maxlength: 150
    },
    gameEngine: {
        type: String,
        required:  [true, 'game engine name is required'],
        minlength: 1,
        maxlength: 100
    },
    platform: {
        type: String,
        required:  [true, 'platform name is required'],
        minlength: 1,
        maxlength: 175
    },
    numberInStock: {
        type: Number,
        required:  [true, 'Number in stock is required'],
        min: 0,
        max:255
    },
    price: {
        type: Number,
        required:  [true, 'Price is required'],
        min: 0,
        max:400
    },
    releaseDate: {
        type: Date,
        min: '1970-01-01',
        required:  [true, 'release date is required'],
        validate:{
            validator: function(rd){
                dateNow  = Date.now();
                //validates the date, if its within current date
                // return  (moment(rd, 'YYYY-MM-DD').isSameOrBefore(dateNow));
                return  (dayjs(rd, 'YYYY-MM-DD').isSameOrBefore(dateNow));
            },
            message:'Invalid date - release date should be within current date'
        }
    }
},{ timestamps: true });

//indexing games collection: Compound Index
gameSchema.index({'_id':1,'releaseDate': -1});

// model from game schema
const Game = mongoose.model('Game', gameSchema);

exports.Game = Game;