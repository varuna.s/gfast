const mongoose = require('mongoose');

//mongoose schema of order
const orderSchema = new mongoose.Schema({
    game:{
        type:new mongoose.Schema({
            name: {
                type: String,
                required: true,
                minlength: 1,
                maxlength: 150
            }
        }) 
    },
    user:{
        type:new mongoose.Schema({
            name:{
                type: String,
                minlength: 3,
                maxlength: 50,
                required: true
            }
        })
    },
    price: {
        type: Number,
        // required: true,
        min: 0,
        max:400
    }
},{ timestamps: true });

//indexing orders collection: Single Field
orderSchema.index({'orderDate': -1});

// model from order schema
const Order = mongoose.model('Order', orderSchema);

exports.orderSchema = orderSchema;
exports.Order = Order;