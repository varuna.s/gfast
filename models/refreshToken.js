const mongoose = require('mongoose');

//schema for refreshToken
const refreshTokenSchema = mongoose.Schema({
    userId: {
        type: 'string',
        required: true,
        unique: true
    },
    refreshToken: {
        type: 'string',
        required: true
    }
}, {timestamps: true});

refreshTokenSchema.index({ userId: 1, refreshToken: 1 }, { unique: true });

//model for refreshToken
const RefreshToken = mongoose.model('RefreshToken', refreshTokenSchema);

module.exports = RefreshToken;