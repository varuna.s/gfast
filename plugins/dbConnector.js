const fastifyPlugin = require('fastify-plugin');
const mongoose = require('mongoose');

//db connector plugin
async function dbConnector (fastify, options, next) {
    try{
        const db = process.env.DB_STRING;
        mongoose.connect(db, { useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex :true, useFindAndModify: false })
            .then(() => fastify.log.info(`Connected to ${db}`));
    }
    catch(err)
    {
        fastify.log.info(err);
        fastify.log.info(`Error connecting to ${process.env.DB_STRING}`);
    }
}

// Wrapping a plugin function with fastify-plugin exposes the decorators    
// and hooks, declared inside the plugin to the parent scope.
module.exports = fastifyPlugin(dbConnector);