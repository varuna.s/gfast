const fastifyPlugin = require('fastify-plugin');
const jwt = require('jsonwebtoken');
const tiny = require('tiny-json-http');
const fs = require('fs');

module.exports = fastifyPlugin( function (fastify, options, next) {
  //avoids mutation of the request object after it has already been instantiated
  fastify.decorateRequest('user', null);

  //authentication decorator
  fastify.decorate("authn", async function (request, reply) {
  
  const token = request.headers['x-auth-token'];
  if(!token)
      return reply.status(401).send({message:'Access Denied. No valid token'});
  try{
  const privateKey = await fs.promises.readFile(process.env.JWT_PRIVATE_KEY_PATH);  
  const decoded =  jwt.verify(token, privateKey, {algorithms:'PS256'});
  request.user = decoded;
  }
  catch(ex)
  {
      request.log.info(ex);
      throw ex;
  }
  });

  //authorization decorator
  fastify.decorate("authz", async function (request, reply) {
    const token = request.headers['x-auth-token'];
    const apiPath = request.routerPath;
    const method = request.method;
    let result;
    let response;
    //options for the policy eval http request
    const options = {
      url:'http://localhost:8181/v1/data/policy',
      data: {
          "input": {
              "api": apiPath,
              "jwt": token,
              "method": method
          }
      },
      headers:{
          'Content-Type': 'application/json'
      }
    }
    //function to request the OPA api endpoints
    //IIFE  
    const isAllowed = await (async function (){
        try {
          response = await tiny.post(options);          
        }
        catch(err){
            request.log.info(err);
            throw new Error("Cannot connect to policy agent");
        }
        if(response.body.result === undefined)
        {
          const err = new Error("No response from policy agent, policy might not have been loaded");
          request.log.info(err);
          throw err;
        }
        result = response.body.result.allow;
        return result;
    })();
    if(!isAllowed){
      request.log.info(`Unauthorized Request - Forbidden`);
      return reply.status(403).send({message:"Unauthorized Request - Forbidden"});
    }
  });
  next();
});