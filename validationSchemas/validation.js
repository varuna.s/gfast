
gameInputProperties = {
    name: {type: 'string', minLength:1, maxLength: 150},
    genre: {type: 'string', minLength:1, maxLength: 150},
    developerName:{type: 'string', minLength:1, maxLength: 150},
    publisherName:{type: 'string', minLength:1, maxLength: 150},
    gameEngine:{type: 'string', minLength:1, maxLength: 100},
    platform:{type: 'string', minLength:1, maxLength: 175},
    numberInStock:{type: 'number',minimum: 0, maximum: 255},
    price:{type: 'number',minimum: 0, maximum: 400},
    releaseDate:{type: 'string', format: 'date'}
};

const signupBodySchema = {
    type: 'object',
    properties: {
        name:{type:'string', minLength:3, maxLength:50},
        email:{type:'string', format: 'email',minLength:6, maxLength:255},
        password:{type:'string', minLength:8, maxLength:255}
    }
}

const loginBodySchema = {
    type: 'object',
    properties: {
        email:{type:'string', format: 'email',minLength:6, maxLength:255},
        password:{type:'string', minLength:8, maxLength:255}
    }
}


const paramsSchema = {
        id: {type: 'string', pattern:"^[a-f0-9]{24}$"}  
}

const queryStringSchema = {
    page: { type: 'integer', minimum: 1},
    limit: { type: 'integer', minimum: 1, maximum: 20}
  }


const requestBodySchema = {
    type: 'object',
    required: ['name', 'genre', 'developerName', 'publisherName', 'gameEngine', 'platform', 'numberInStock', 'price', 'releaseDate'],
    properties: gameInputProperties     
}

const responseMessageSchema={
    type: 'object',
    properties:{
        statusCode:{type: 'number'},
        error:{type:'string'},
        message: {type:'string'}
    }
}

//returning requestBodySchema's properties except "required" property
const patchRequestBodySchema = Object.fromEntries(
    Object.entries(requestBodySchema).filter(([key, value]) => key != 'required'));

const ordersBodySchema = {
    gameId: {type: 'string', pattern:"^[a-f0-9]{24}$"},
    userId: {type: 'string', pattern:"^[a-f0-9]{24}$"} 
}

//input validation schema for the routes and their serialization output schema
const getAllSchema = {
    querystring: queryStringSchema,
    response: {
        200: {
            page: {type:'string'},
            totalPages:{type:'string'},
            games:{
                type:'array',
                items: {
                    type: 'object',
                    properties: {
                        _id: { type:'string'},
                        ...gameInputProperties,
                        createdAt:{ type:'string'},
                        updatedAt:{ type:'string'},
                        __v:{type: 'number'}
                    }
                } 
            }
        } 
    }
}
const getRequestSchema = {
    params: paramsSchema,
    response: {
        200: {
            type: 'object',
            properties: {
                _id: { type:'string'},
                ...gameInputProperties,
                createdAt:{ type:'string'},
                updatedAt:{ type:'string'},
                __v:{type: 'number'}
            }
        },
        404: responseMessageSchema,
        400: responseMessageSchema,
        500: responseMessageSchema
    }
}

const postRequestSchema = {
    body: requestBodySchema,
    response: {
        200: {
            type: 'object',
            properties: {
                _id: { type:'string'},
                ...gameInputProperties,
                __v:{type: 'number'},
                createdAt:{ type:'string'},
                updatedAt:{ type:'string'}
            }
        },
        400: responseMessageSchema,
        401: responseMessageSchema,
        403: responseMessageSchema,
        500: responseMessageSchema 
    }
}
const  putRequestSchema = {
    body: requestBodySchema,
    params : paramsSchema,
    response: {
        200: {
            type: 'object',
            properties: {
                _id: { type:'string'},
                ...gameInputProperties,
                __v:{type: 'number'},
                createdAt:{ type:'string'},
                updatedAt:{ type:'string'}
            }
        },
        400: responseMessageSchema,
        401: responseMessageSchema,
        403: responseMessageSchema,
        404: responseMessageSchema,
        500: responseMessageSchema 
    }
}
const  patchRequestSchema ={
    body: patchRequestBodySchema,
    params : paramsSchema,
    response: {
        200: {
            type: 'object',
            properties: {
                _id: { type:'string'},
                ...gameInputProperties,
                __v:{type: 'number'},
                createdAt:{ type:'string'},
                updatedAt:{ type:'string'}
            }
        },
        400: responseMessageSchema,
        401: responseMessageSchema,
        403: responseMessageSchema,
        404: responseMessageSchema,
        500: responseMessageSchema 
    }
}
const  deleteRequestSchema ={
    params : paramsSchema,
    response: {
        200: {
            type: 'object',
            properties: {
                _id: { type:'string'},
                ...gameInputProperties,
                __v:{type: 'number'},
                createdAt:{ type:'string'},
                updatedAt:{ type:'string'}
            }
        },
        400: responseMessageSchema,
        401: responseMessageSchema,
        403: responseMessageSchema,
        404: responseMessageSchema,
        500: responseMessageSchema
    }
}
const signupSchema = {
    body: signupBodySchema,
    response: {
        200: {
            type: 'object',
            properties: { 
                _id: { type: 'string'},
                name:{ type: 'string'},
                email: { type: 'string', format: 'email'},
                createdAt:{ type:'string'},
                updatedAt:{ type:'string'}
            }
        },
        400: responseMessageSchema,
        500: responseMessageSchema
    }
}
const getUsersSchema = {
    querystring: queryStringSchema,
    response: {
        200:{ 
            page: {type:'string'},
            totalPages:{type:'string'},
            users:{
                type: 'array',
                items: {
                    type: 'object',
                    properties:{
                        _id: { type: 'string'},
                        name:{ type: 'string'},
                        email: { type: 'string'},
                        isAdmin:{type: 'boolean'},
                        createdAt:{ type:'string'},
                        updatedAt:{ type:'string'}
                    }
                } 
            }
        },
    401: responseMessageSchema,
    403: responseMessageSchema,
    500: responseMessageSchema 
    }
}
const getMeSchema = {
    response: {
        200: {
            type: 'object',
            properties:{
                _id: { type: 'string'},
                name:{ type: 'string'},
                email: { type: 'string'},
                isAdmin:{type: 'boolean'},
                createdAt:{ type:'string'},
                updatedAt:{ type:'string'}
            }
        },
        401: responseMessageSchema,
        500: responseMessageSchema 
    }
}

const refreshTokenSchema ={
    body:{
        refreshToken:{
            type: 'string',
            pattern: "^[a-zA-Z0-9_-]{21}$"
        }
    },
    response:{
        200: responseMessageSchema,
        400: responseMessageSchema,
        404: responseMessageSchema,
        500: responseMessageSchema    
    }
}

const logoutSchema ={
    params:{
        refreshToken:{
            type: 'string',
            pattern: "^[a-zA-Z0-9_-]{21}$"
        }
    },
    response:{
        200: responseMessageSchema,
        400: responseMessageSchema,
        401: responseMessageSchema,
        500: responseMessageSchema    
    }
}

const loginSchema = {
    body: loginBodySchema,
    response: {
        200: {
            type: 'object',
            properties:{
                _id: { type: 'string'},
                name:{ type: 'string'},
                email: { type: 'string'},
                refreshToken: { type: 'string'}
            }
        },
        400: responseMessageSchema,
        401: responseMessageSchema,
        500: responseMessageSchema  
    } 
}

const ordersPostSchema = {
    body: ordersBodySchema,
    response:{
        200:{ 
            type: 'array',
            items:{
                type: 'object',
                properties:{
                    _id: { type: 'string'},
                    game:{ 
                        type: 'object',
                        properties:{
                            _id: { type: 'string'},
                            name:{ type: 'string'},
                        }
                    },
                    user:{ 
                        type: 'object',
                        properties:{
                            _id: { type: 'string'},
                            name:{ type: 'string'}
                        }
                    },
                        price:{type:'string'}
                }
            }
        },
        400: responseMessageSchema,
        401: responseMessageSchema,
        500: responseMessageSchema 
    }
}

const getOrdersResponseSchema = {
    200: {
        type: 'array',
        items: {
            type: 'object',
            properties:{
                orders:{
                    type:'array',
                    items:{
                        type: 'object',
                        properties:{
                            _id: { type: 'string'},
                            game:{
                                type: 'object',
                                properties:{
                                    name:{ type: 'string'}
                                }
                            },
                            user:{
                                type: 'object',
                                properties:{
                                    name:{ type: 'string'}
                                }
                            },
                            price:{ type: 'number'},
                            createAt:{ type: 'string'},
                            updatedAt:{ type: 'string'}
                        }
                    }       
                },    
                metadata:{
                    type:'array',
                    items:{
                        type:'object',
                        properties:{
                            total:{ type: 'string'},
                            page:{ type: 'string'},
                            totalPages:{ type: 'string'}
                        }
                    }
                }     
            }
        }
    },
    401: responseMessageSchema,
    403: responseMessageSchema,
    500: responseMessageSchema, 
}    


const getOrdersSchema = {
    response: getOrdersResponseSchema,
    querystring: queryStringSchema,
}

const myOrdersSchema = {
    response: getOrdersResponseSchema
}

// /api/games
exports.getAllSchema = getAllSchema;
exports.getRequestSchema = getRequestSchema;
exports.postRequestSchema = postRequestSchema;
exports.putRequestSchema = putRequestSchema;
exports.patchRequestSchema = patchRequestSchema;
exports.deleteRequestSchema = deleteRequestSchema;
// /api/users
exports.signupSchema = signupSchema;
exports.getUsersSchema = getUsersSchema;
exports.getMeSchema = getMeSchema;
exports.refreshTokenSchema = refreshTokenSchema;
exports.logoutSchema = logoutSchema; 
// /api/authn
exports.loginSchema = loginSchema;

// /api/games/orders
exports.ordersPostSchema = ordersPostSchema;
exports.getOrdersSchema = getOrdersSchema;
exports.myOrdersSchema = myOrdersSchema;