const {ordersPostSchema, getOrdersSchema, myOrdersSchema} = require('../validationSchemas/validation');

const ordersPostOptions = {
    schema: ordersPostSchema
}

const getOrdersOptions = {
    schema: getOrdersSchema
}

const myOrdersOptions = {
    schema: myOrdersSchema
}

exports.ordersPostOptions = ordersPostOptions;
exports.getOrdersOptions = getOrdersOptions;
exports.myOrdersOptions = myOrdersOptions;