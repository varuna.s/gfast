const {signupSchema, getUsersSchema, getMeSchema, logoutSchema, refreshTokenSchema} = require('../validationSchemas/validation');

//GET /api/users/me
const getMeOptions = {
    schema: getMeSchema
} 

//GET /api/users/
const getOptions = {
   schema: getUsersSchema
}

//POST /api/users signup
const postOptions = {
    schema :signupSchema
}

//POST /api/users/:refreshToken/logout
const logoutOptions = {
    schema : logoutSchema
}
//POST /api/users/refreshToken
const refreshTokenOptions = {
    schema : refreshTokenSchema
}

exports.getMeOptions = getMeOptions;
exports.getOptions = getOptions;
exports.postOptions = postOptions;
exports.logoutOptions = logoutOptions;
exports.refreshTokenOptions = refreshTokenOptions;