const {getAllSchema,getRequestSchema, postRequestSchema, putRequestSchema, patchRequestSchema,deleteRequestSchema} = require('../validationSchemas/validation');

//route options for the routes
const getAllOptions = {
    schema:getAllSchema
}

const getOptions = {
    schema: getRequestSchema
}

const postOptions = {
    schema: postRequestSchema
}

const putOptions = {
    schema: putRequestSchema
}
const patchOptions = {
    schema: patchRequestSchema
}

const deleteOptions = {
    schema: deleteRequestSchema
}

exports.getAllOptions = getAllOptions;
exports.getOptions = getOptions;
exports.postOptions = postOptions;
exports.putOptions = putOptions;
exports.patchOptions = patchOptions;
exports.deleteOptions = deleteOptions;