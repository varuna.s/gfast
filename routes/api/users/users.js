const fs = require('fs');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const _ = require('lodash');
const {getMeOptions, getOptions, postOptions, logoutOptions, refreshTokenOptions} = require('../../../routeOptions/users');
const {User} = require('../../../models/user');
const RefreshToken = require('../../../models/refreshToken');

async function routes(fastify, options){

    //setting the authentication(authn) and authorization(authz) decorators for the route options
    getMeOptions.preHandler = fastify.auth([fastify.authn],{relation: 'and'});
    getOptions.preHandler = fastify.auth([fastify.authn, fastify.authz],{relation: 'and'});
    logoutOptions.preHandler = fastify.auth([fastify.authn],{relation: 'and'});
    //user details of the current logged in user
    fastify.get('/me', getMeOptions, async (request, reply) => {
      try{
        let user = await User.findOne({_id: request.user._id}).select('-password -__v');
        if(!user) return reply.status(500).send({ message: `Could not get user details`});

        reply.send(user);
      }
      catch(err){
        request.log.info(err);
        throw new Error(err.message);
      } 
      });
    
    //get all users by admin only
    fastify.get('/', getOptions, async (request, reply) => {
      try {
        let page = request.query.page ? request.query.page: 1; 
        let limit = request.query.limit ? request.query.limit : 5;
        let users;
        let totalUsersCount;
        const skip = (page - 1 ) * limit;

        [users, totalUsersCount] = await Promise.all([
          User.find({}).skip(skip).limit(limit),
          User.countDocuments({})
        ]);
      reply.send({
        page:page,
        totalPages:Math.ceil(totalUsersCount/limit),
        users:users
      });
      }
      catch(err){
        request.log.info(err);
        throw new Error(err.message);
      }
    });

    //sign up 
    fastify.post('/', postOptions, async (request, reply) => {
      try{
        let user = await User.findOne({email: request.body.email});
        if(user)
          return reply.status(400).send({ message: "User already registered with this email"});

        user = new User(_.pick(request.body, ['name','email','password']));
        //hashing password by adding salt
        const salt = await bcrypt.genSalt(10);
        user.password= await bcrypt.hash(user.password, salt);

        await user.save();
        reply.send(_.pick(user, ['_id', 'name','email']));
      }
      catch(err){
        request.log.info(err);
        throw new Error(err.message);
      }
    });

    //refresh token
    fastify.post('/refresh-token', refreshTokenOptions, async (request, reply) => {
      try{
        const token = request.headers['x-auth-token'];
        const {refreshToken} = request.body;
        if(!token)  return reply.status(400).send({message:`No Access token header`});
        
        //verify the access token ignoring the expiration
        const privateKey = await fs.promises.readFile(process.env.JWT_PRIVATE_KEY_PATH);  
        const decoded = jwt.verify(token, privateKey, {algorithms:'PS256', ignoreExpiration: true});

        //to check if refresh token is valid 
        const refreshTokenInDb = await RefreshToken.findOne({userId: decoded._id, refreshToken}).lean();
        if(!refreshTokenInDb) return reply.status(404).send({message:`Invalid refresh token`});

        const newToken = await new User({_id: decoded._id, isAdmin: decoded.isAdmin}).generateAuthToken();

        return reply
          .header('x-auth-token', newToken)
          .send({message: `New access token created`});
        
      }
      catch(err)
      {
        request.log.info(err);
        throw new Error(err.message);
      }
    });

    fastify.delete('/:refreshToken/logout', logoutOptions, async (request, reply) => {
      try{
        const token = request.headers['x-auth-token'];
        const {refreshToken} = request.params;
        if(!token)  return reply.status(400).send({message:`No access token header`});

        //verify the access token ignoring the expiration
        const privateKey = await fs.promises.readFile(process.env.JWT_PRIVATE_KEY_PATH);  
        const decoded = jwt.verify(token, privateKey, {algorithms:'PS256', ignoreExpiration: true});

        const isTokenDeleted = await RefreshToken.findOneAndDelete({userId: decoded._id, refreshToken}).lean();
        if(!isTokenDeleted) return reply.send({message:`Invalid refresh token`});

        return reply.send({message:`Refresh token deleted`});
                
      }
      catch(err){
        request.log.info(err);
        throw new Error(err.message);
      }
    });
}

module.exports = routes;