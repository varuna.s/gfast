const {Game} = require('../../../../models/game');
const {User} = require('../../../../models/user');
const {Order} = require('../../../../models/order');
const Mongoose = require('mongoose');
const {ordersPostOptions, getOrdersOptions, myOrdersOptions} = require('../../../../routeOptions/orders');

async function routes(fastify, options){

    //setting the authentication(authn) and authorization(authz) decorators as preHandlers for the selected route options
    getOrdersOptions.preHandler = fastify.auth([fastify.authn, fastify.authz], {relation: 'and'});
    ordersPostOptions.preHandler = fastify.auth([fastify.authn]);
    myOrdersOptions.preHandler = fastify.auth([fastify.authn]);

    //GET all orders by admin only
    fastify.get('/', async (request, reply) => {
        try{
            const page = parseInt(request.query.page ? request.query.page: 1); 
            const limit =  parseInt(request.query.limit ? request.query.limit : 5);
            const skip = (page - 1 ) * limit;
            //aggregate  pipeline to get all the order details with order count
            let orders = await Order.aggregate([    
                
                { $facet: {
                    orders: [ {$skip: skip}, {$limit: limit },{ $sort: { orderDate: -1}},{ $project : { __v: 0, game :{_id:0}, user :{_id:0}}}],
                    metadata: [ { $count: "total" }, {$unwind: '$total'},{ $addFields: { page: page, totalPages:{ $ceil: {$divide: [ '$total' , limit]}}}}]
                }},
            ]);
            
            return reply.send(orders);
        }
        catch(err)
        {
            request.log.info(err);
            throw new Error(err.message);
        }
    });
    //GET all the orders by currently logged in user
    fastify.get('/my-orders', myOrdersOptions, async (request, reply) => {
        try{
            const page = parseInt(request.query.page ? request.query.page: 1); 
            const limit =  parseInt(request.query.limit ? request.query.limit : 5);
            const skip = (page - 1 ) * limit;
            let orders = await Order.aggregate([
                { $facet: {
                    orders: [ {$skip: skip}, {$limit: limit },{ $sort: { orderDate: -1}},{ $project : { __v: 0, game :{_id:0}, user :{_id:0}}}],
                    metadata: [ { $count: "total" }, {$unwind: '$total'},{ $addFields: { page: page, totalPages:{ $ceil: {$divide: [ '$total' , limit]}}}}]
                }}
            ]);
            return reply.send(orders);
        }
        catch(err)
        {
            request.log.info(err);
            throw new Error(err.message);
        }
    });

    //POST
    fastify.post('/', ordersPostOptions, async (request, reply) => {
        let order;
        
        let game = await Game.findOne({_id: request.body.gameId});
        if(!game)  return reply.status(404).send({message:`Game with the id: ${request.body.gameId} not found`});

        if(game.numberInStock === 0) return reply.status(400).send({message:`Game not in stock`});

        const user = await User.findOne({_id: request.body.userId}).lean();
        if(!user)  return reply.status(404).send({message:`User with the id: ${request.body.userId} not found`});

        const session =  await Mongoose.startSession();
        try {
            await session.withTransaction( async () => {
                order = await Order.create([{
                    game: {
                        _id: game._id,
                        name: game.name
                    },
                    user: {
                        _id: user._id,
                        name: user.name
                    },
                    price: game.price
                }], { session });
                game.numberInStock--;
                await game.save({session});
            });
            reply.send(order);           
        }
        catch(err)
        {
            request.log.info(err);
            reply.send(err);
            throw err;
        }
        finally{
            session.endSession();
            return;    
        }
});

}

module.exports = routes;