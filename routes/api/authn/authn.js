const bcrypt = require('bcrypt');
const _ = require('lodash');
const {nanoid} = require('nanoid');
const {loginOptions} = require('../../../routeOptions/authn');
const {User} = require('../../../models/user');
const RefreshToken = require('../../../models/refreshToken');

module.exports = async function routes(fastify, options){
        //Login
        fastify.post('/', loginOptions, async (request, reply) => {
            try{
                const user = await User.findOne({email: request.body.email});
                if(!user)  return reply.status(401).send({message: 'Invalid email or password'});

                const isPasswordValid= await bcrypt.compare(request.body.password, user.password);
                if(!isPasswordValid) return reply.status(401).send({message:'Invalid email or password'});

                const refreshToken = nanoid();
                const [token,] = await Promise.all([
                    user.generateAuthToken(),
                    RefreshToken.findOneAndUpdate({userId: user._id},{$set: {refreshToken}},{upsert: true})
                ]);
                    
                return reply
                    .header('x-auth-token', token)
                    .send({
                        _id: user._id,
                        name: user.name,
                        email: user.email,
                        refreshToken
                    });
            }
            catch(err){
                request.log.info(err);
                throw new Error(err.message);
            }
        });
    }